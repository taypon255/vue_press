---
title: aws_IasS,SaaS,PaaSノウハウ
date: 2019-09-19
description: 小ネタを気が向いたら追加する
---

## セキュリティグループ

インスタンスの仮想ファイアウォールとして使用できる機能でインスタンス1つにつき最大5つまで設定することができる。  
GUI上で設定できるなど操作性に優れているためLinuxであれば`iptables`や`firewalld`を使うよりも保守性がいいかもしれない。  
ただし設定ファイルで制御できないため、リポジトリやドキュメント管理には向かない

- セキュリティグループの設定値に他セキュリティグループをincludeさせることも可能。

### 参考リンク
- [VPC のセキュリティグループ](https://docs.aws.amazon.com/ja_jp/vpc/latest/userguide/VPC_SecurityGroups.html)