---
title: react_基礎知識
date: 2019-09-19
description: ReactJsの構文やファイル構成等の基礎知識
---

## 概要

ReactではVueと同じようにコンポーネントファイルをHTMLに変換させることでWEBページ表示をさせるフレームワーク。  
React内で記述するHTMLの部分は`JSX`という記法で記述し、コンパイルによって変換が行われる。  


<img src="https://idobata.io/att/uploads/attachment/image/1135728/5066fff8-4a84-4b77-b6e4-c105b601d9e3/image.png">

また特徴として、`State`、`props`などの可変データを用いて非同期処理による表示の切り替えを行うことができる。

## コンポーネントクラスの作成
コンポーネントファイルを作成するには3つの手順が必要。

1. Reactのimport
1. React.Componentを継承するクラス定義
1. JSXを戻り値とする`render`メソッド

```js
import React from 'react';  //Reactのimport

class App extends React.Component{  //React.Componentを継承するクラス定義
  render(){  //JSXを戻り値とする`render`メソッド
    //JSX
  }

}

```
## コンポーネントの呼び出し
1. 呼び出したいコンポーネントを`export`する
```js
export default [className];
```

1. 呼び出しもとコンポーネントで`import`する

```js
import [className] from 'classNameパス'
```

1. JSX内で`<コンポーネント名 />`のように記述し呼び出す

```js
class App extends React.Component {
  render() {
    return (
      <div>
        <[className] />  //呼び出し
      </div>
```

## 親コンポーネントから子コンポーネントへのデータ渡し
`props`を使用し子コンポーネントへデータを渡す。  
`props`は`props名=値`の形でコンポーネントを呼び出す箇所で渡す必要がある。  
以下例では、`name`と`image`という2つのpropsを渡すことができる。

[ 親コンポーネント ]
```js
<ParentConpornent
  name  = 'データ１'
  image = 'https://…'
/>
<ParentConpornent
  name  = 'データ2'
  image = 'https://…'
/>
```

渡されたpropsは`this.props`で取得でき、`{ props名: 値}`というオブジェクトになるので`this.props.props名`の形で呼び出すことができる。  
vueJsでは属性内で呼び出す際はマスタッシュ構文を使えないが、ReactJsでは可能。


```js
class Language extends React.Component {
  render() {
    return (
        <div className='name'>{ this.props.name }</div>
        <img 
          className='image'
          src={this.props.image}
          />
        
      </div>
    );
  }
}
```

## 繰り返し処理
JSX内でループさせる場合はmapメソッドを使うことで実現できる。  [この記事](https://qiita.com/konojunya/items/cb026a2aa3df1837d587#react%E3%81%A7%E3%83%AB%E3%83%BC%E3%83%97%E3%81%99%E3%82%8B%E3%81%AB%E3%81%AF)が参考になる。
