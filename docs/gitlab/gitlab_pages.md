---
title: gitlab_gitlab-pages
date: 2019-09-23
description: gitlab pagesの概要と使い方
---

## Gitlab Pages
Gitlab PagesとはGitlab上のプロジェクトに作成された静的ページをホスティングしてくれるサービス。  
プロジェクトの`public`ディレクトリ配下にある静的コンテンツを公開しWEB閲覧することができる。  

似たようなサービスとして`Github Pages`があるが、Gitlubではホスティングサーバ側でビルドできさらにそれらを`Gitlab CI`を使用することで手軽に自動化することができたり、Githubのようにデプロイ用のプロジェクトを作成する必要がないなど、公開から運用までサポートが非常に手厚く扱いやすい。

[こちら](https://www.gitlab.jp/comparison/gitlab-pages-vs-github-pages.html)のページで`Github Pages`と`Gitlab Pages`の比較をしている。

## 使い方
具体的にGitlab Pagesを使いWEBページを公開するまでの方法を記述する。

#### プロジェクト権限設定
公開するプロジェクトは権限を`public`に変更する必要がある。  
ソースコードは公開せずあくまでWEB閲覧のみをできるようにしたい場合は以下のように設定することで実現可能。

1. プロジェクトページのサイドバーで画面遷移
`設定` => `一般` => `permisson`
1. `Project visibility`を`public`に設定
1. `Repository`を`Only My Project Members`に設定（その他項目も必要に応じて同様に設定する）

## ビルド自動化
リポジトリのルートに`.gitlab-ci.yml`を作成し、masterが修正された場合に自動的にGitlab Dockerでソースをビルドするように設定する。  
本ページはフレームワークに`vuepress`を使用しているため、`vuepress build`により作成したソースをGitlab Docker上の`public`ディレクトリにデプロイするように設定した。  

```yml
image: node:9.11.1

pages:
  cache:
    paths:
    - node_modules/

  script:
  - npm i -g vuepress
  - vuepress build docs
  - mkdir public
  - cp -pr build/* public
  artifacts:
    paths:
    - public
  only:
  - master
```

## 参考リンク

- [VuePressのブログをカスタムドメイン+SSL対応させたGitLab Pagesにデプロイする](https://dotstud.io/blog/gitlab-vuepress-custom-domain/)
- [GitLabでドキュメントのビルドとホスティング](https://miyakogi.github.io/blog/20160429/gitlab-pages-ci.html#.XYiXjUb7Ryw)
- [GitLabページとGitLab CI](https://vuepress.vuejs.org/guide/deploy.html#github-pages)
- [《Vue.js》VuePress と GitLab CI を使って自動ビルド & デプロイされるドキュメント管理環境をサクッと作る方法。](https://qiita.com/uto-usui/items/17045bcdad4beb07fbbf)