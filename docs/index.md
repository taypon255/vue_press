---
home: true
heroImage: /index.png
footer:  2019 TairaU
---

<body>
  <div v-for="midashi in midashis">
    <SkillType :midashi="midashi" :skillItems="frontItems"/>
  </div>
</body>

<script>
import SkillType from "./.vuepress/components/SkillType";

export default {
    data() {
    return {
      //見出しオブジェクト
      midashis: [
        { title: "FRONT", 
          subTitle: "フロントエンド言語・フレームワーク",
          skillItems: [
            { name: "VueJs", image: "./skill/vuepress.png", url: "./vuejs/" },
            { name: "ReactJs", image: "./skill/react.png", url: "./react/" },
            { name: "VuePress", image: "./skill/vuejs.png", url: "./vue_press/" }
          ]
        },
        { title: "BACK", 
          subTitle: "バックエンド言語・フレームワーク",
          skillItems: [
            { name: "Java", image: "./skill/java.png", url: "./java/" },
            { name: "MySQL", image: "./skill/mysql.png", url: "./mysql/" },
            { name: "Comming soon...",  url: "" },
          ]
        },
        { title: "INFRA", 
          subTitle: "サーバー・ミドルウェア",
          skillItems: [
            { name: "Linux", image: "./skill/linux.png", url: "./linux/" },
            { name: "AWS", image: "./skill/aws.png", url: "./aws/" },
            { name: "NetWork", image: "./skill/network.png",url: "./network/" },
          ]
        }
      ],
    };
  }

}
</script>

<style>


body {
    font-size: 15px;
    font-family: Avenir,"Helvetica Neue",Helvetica,Arial,"Hiragino Sans","ヒラギノ角ゴシック",YuGothic,"Yu Gothic","メイリオ",Meiryo,"ＭＳ Ｐゴシック","MS PGothic";
    line-height: 1.6;
    position: relative;
    width: 100%;
}

h2 {
  
  padding-bottom: 1em;
}
/* http://meyerweb.com/eric/tools/css/reset/ 
   v2.0 | 20110126
   License: none (public domain)
   リセットCSS
*/
 
html, body, div, span, applet, object, iframe,
h1, h2, h3, h4, h5, h6, p, blockquote, pre,
a, abbr, acronym, address, big, cite, code,
del, dfn, em, img, ins, kbd, q, s, samp,
small, strike, strong, sub, sup, tt, var,
b, u, i, center,
dl, dt, dd, ol, ul, li,
fieldset, form, label, legend,
table, caption, tbody, tfoot, thead, tr, th, td,
article, aside, canvas, details, embed, 
figure, figcaption, footer, header, hgroup, 
menu, nav, output, ruby, section, summary,
time, mark, audio, video {
	margin: 0;
	padding: 0;
	border: 0;
	font-size: 100%;
	font: inherit;
	vertical-align: baseline;
}
/* HTML5 display-role reset for older browsers */
article, aside, details, figcaption, figure, 
footer, header, hgroup, menu, nav, section {
	display: block;
}
body {
	line-height: 1;
}
ol, ul {
	list-style: none;
}
blockquote, q {
	quotes: none;
}
blockquote:before, blockquote:after,
q:before, q:after {
	content: '';
	content: none;
}
table {
	border-collapse: collapse;
	border-spacing: 0;
}

</style>
