---
title: vuejs_実装ノウハウ
date: 2019-09-19
description: vuepressでの双方向データバインディング等の主要実装方法ノウハウ
---

## コンポーネントファイルの構成
Vuejsのコンポーネントのファイル（*.vue）の構成は以下のようになっている

[ *.vue ]
```html
<templete>
  テンプレート
</templete>

<script>
  ロジック
</script>

<style>
  スタイル
</style>
```

ロジックは`データ`と`メソッド`の二つで構成される。

```html
<script>
export default {
  data() {
    return {
      データ
    };
  },
  methods: {
    メソッド
  }
};
</script>
```

## コンポーネント間のコミュニケーション
Vue.jsはコンポーネントベースのフレームワークなので、複数のコンポーネントでアプリを構成することができる。  
２つのコンポーネント（親子関係）がある場合、コンポーネント間でどのようにデータのやり取りを行うかを理解することが重要となる。

### コンポーネントの呼び出し
親コンポーネントが子コンポーネントを呼び出す場合はコンポーネントの登録が必要になる。  
以下ではimportを使用し、App.vueがChild.vueを呼び出している。

テンプレートでは`<child>`でカスタム要素を使用している。

- App.vue
```html
<templete>
<child v-bind:pval ="parentValue" v-on:add ="addValueParent($event)">
</child>
</templete>

<script>
import Child from './components/ Child'
  
  components: { 
    'child': Child
  },
```

### データの受け渡し
Vuejsでは`props`というプロパティを使いデータをコンポーネント間でやり取りする。

- 親コンポーネントから子コンポーネントにデータを渡す
以下例では親コンポーネントが`parent`というプロパティを使って`data: {}`で定義したデータを子コンポーネントに渡している。

[ 親 ] 
```js
<child v-bind:parent ="データ"></child>
```

そして子コンポーネントは上記で渡された`parent`というプロパティを使ってデータを受け取っている

[ 子 ]

```js
props: ['parent']
```

- 子コンポーネントから親コンポーネントにデータを渡す
以下例では`$emit`（引数で指定したイベントをを発火させる、第二引数はコールバックの引数となる）メソッドで親メソッドを発火させ、`$event`に引数のデータを渡している。


[ 子 ]
```js
this.$emit("child", データ);
```

[ 親 ]
```js
<child v-on:child ="親メソッド($event)"></child>
```

### サンプル

[App.vue]

```js
<template>
  <child v-bind:pval="parentValue" v-on:add="addValueParent($event)"></child>
</template>

<script>
import Child from "./components/Child";
export default {
  data() {
    return { parentValue: 100 };
  },
  components: { child: Child },
  methods: {
    addValueParent(value) {
      this.parentValue += value;
    }
  }
};
</script>
```

[Child.vue]

```js
<template>
  <div>
    {{ pval }}
    <button v-on:click="addValueChild()">ADD</button>
  </div>
</template>

<script>
export default {
  props: ["pval"],
  methods: {
    addValueChild() {
      this.$emit("add", 10);
    }
  }
};
</script>
```


### 参考リンク
- [コンポーネントの登録](https://jp.vuejs.org/v2/guide/components-registration.html)

## データバインディング

Vuejsのテンプレートでは`{{}}（マスタッシュ構文）`を使ってデータにアクセスすることができる。  
  
単にテキストとして参照する場合はマスタッシュ構文でいいのだが、属性に対して値を設定するような場合は`{{}}`を使用できないので`v-bindディレクティブ`を使用する。  

v-bindディレクティブの使用方法例

```html
v-bind:属性="値"
または
:属性="値"
```

`v-bind:`は`:`に省略できる

データバインディングの使用例

```html
<template>
  <div>
    <h3>{{ title }}</h3>
    <ul>
      <li>
        <a v-bind:href="url1">Vue</a>
      </li>
      <li>
        <a :href="url2">Vuetify</a>
      </li>
    </ul>
  </div>
</template>

<script>
export default {
  data() {
    return {
      title: "Vue sites",
      url1: "https://vuejs.org/",
      url2: "https://vuetifyjs.com/"
    };
  }
};
</script>


<style>
#app {
  font-family: "Avenir", Helvetica, Arial, sans-serif;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  text-align: center;
  color: #2c3e50;
  margin-top: 60px;
}
</style>
```

## 双方向データバインディング
双方向データバインディングとは、モデルとUI要素が互いに同期しあうバインディングのこと。  
  
以下のようにモデル側で初期値が設定された`changeResult`もUI側で変更が加わればモデルの値を更新させることができる。

```html
<input v-bind:value="changeResult" v-on:change="onChange($event)" placeholder="change" />
  {{ changeResult }}

~~~
<script>
export default {
  data() {
    return {
      changeResult: "change default value"
    }
  },
  methods: {
    onChange(e) {
      this.changeResult = e.target.value;
    },
  }
};
</script>
```

さらにVuejsでは双方向バインディングを実現する際に`v-modelディレクティブ`を使用することで、メソッドのが不要になりコードの記述を省略することができる。

```html
<input v-model="vmodelResult" placeholder="v-model" />
{{ vmodelResult }}

~~~
<script>
export default {
  data() {
    return {
      vmodelResult: "vmodel default value"
    }
  },
};
</script>
```

### サンプル

```html
<template>
  <div>
    <input v-bind:value="changeResult" v-on:change="onChange($event)" placeholder="change" />
    {{ changeResult }}
    <br />
    <input v-bind:value="inputResult" @input="onInput($event)" placeholder="input" />
    {{ inputResult }}
    <br />
    <input v-bind:value="keyupResult" @keyup="onKeyup($event)" placeholder="keyup" />
    {{ keyupResult }}
    <br />
    <input v-model="vmodelResult" placeholder="v-model" />
    {{ vmodelResult }}
  </div>
</template>

<script>
export default {
  data() {
    return {
      changeResult: "change default value",
      inputResult: "input default value",
      keyupResult: "keyup default value",
      vmodelResult: "vmodel default value"
    };
  },
  methods: {
    onChange(e) {
      this.changeResult = e.target.value;
    },
    onInput(e) {
      this.inputResult = e.target.value;
    },
    onKeyup(e) {
      this.keyupResult = e.target.value;
    }
  }
};
</script>
```

### 参考リンク
- [フォーム入力バインディング](https://jp.vuejs.org/v2/guide/forms.html)
- [双方向-Two-way-バインディング](https://v1-jp.vuejs.org/guide/index.html#%E5%8F%8C%E6%96%B9%E5%90%91-Two-way-%E3%83%90%E3%82%A4%E3%83%B3%E3%83%87%E3%82%A3%E3%83%B3%E3%82%B0)

## イベント処理
VuejsでDOMのイベントを取得するには`v-onディレクティブ`を使用する。  
`v-onディレクティブ`次のように表記して使用することができる。

```html
v-on:イベント名="メソッド"
または
@:イベント名="メソッド"
```

`v-on:`は`@`に省略できる

たとえば以下のように記述した場合、ボタンにclick属性が指定されているので、`onClick()`メソッドが呼ばれることになる

```html
<button v-on:click ="onClick()"> click </button>

~~~~~
<script>
export default {
  methods: {
    onClick() {
      this.clickResult = "clicked";
    }
  }
}  
</script>
```

またVuejsでは特殊な変数が用意されており、`$event`を使うことでイベントオブジェクトをメソッドに渡すことができる。  
  
以下の例では`$event`を使い`onInput`メソッドにイベントオブジェクトを渡している

```html
<input @input="onInput($event)" placeholder="input" />
  {{ inputResult }}

~~~

<script>
export default {
  data() {
    return {
      inputResult: ""
    };
  },
  methods: {
    onInput(e) {
      this.inputResult = e.target.value;
    }
  }
};
</script>
```

### サンプル

```html
<template>
  <div>
    <button v-on:click="onClick()">click</button>
    {{ clickResult }}
    <br />
    <input v-on:change="onChange($event)" placeholder="change" />
    {{ changeResult }}
    <br />
    <input @input="onInput($event)" placeholder="input" />
    {{ inputResult }}
    <br />
    <input @keyup="onKeyup($event)" placeholder="keyup" />
    {{ keyupResult }}
  </div>
</template>

<script>
export default {
  data() {
    return {
      clickResult: "",
      changeResult: "",
      inputResult: "",
      keyupResult: ""
    };
  },
  methods: {
    onClick() {
      this.clickResult = "clicked";
    },
    onChange(e) {
      this.changeResult = e.target.value;
    },
    onInput(e) {
      this.inputResult = e.target.value;
    },
    onKeyup(e) {
      this.keyupResult = e.target.value;
    }
  }
};
</script>
```

### 参考リンク
- [メソッドとイベントハンドリング](https://v1-jp.vuejs.org/guide/events.html)

## リスト表示、繰り返し処理
Vuejsでは`v-forディレクティブ`使用することで反復処理を行うことができる。  
ディレクティブは`item in items`の形式で記述する。

```html
<ul id="example-1">
  <li v-for="item in items">
    {{ item.message }}
  </li>
</ul>
```

```js
var example1 = new Vue({
  el: '#example-1',
  data: {
    items: [
      { message: 'Foo' },
      { message: 'Bar' }
    ]
  }
})
```

出力結果

```
・Foo
・Bar
```

また`v-forディレクティブ`はオブジェクトに対しても反復処理を実現できる。

```js
<ul id="v-for-object" class="demo">
  <li v-for="value in object">
    {{ value }}
  </li>
</ul>
```

```js
new Vue({
  el: '#v-for-object',
  data: {
    object: {
      title: 'How to do lists in Vue',
      author: 'Jane Doe',
      publishedAt: '2016-04-10'
    }
  }
})
```

出力結果

```
・How to do lists in Vue
・Jane Doe
・2016-04-10
```

### サンプル
```html
<template>
  <ul>
    <li v-for="todo in todos" v-bind:key="todo.id">
      {{ todo.text }}
      <button v-on:click="deleteTodo(todo.id)">x</button>
    </li>
  </ul>
</template>

<script>
export default {
  data() {
    return {
      todos: [
        { id: "1", text: "todo 1" },
        { id: "2", text: "todo 2" },
        { id: "3", text: "todo 3" }
      ]
    };
  },
  methods: {
    deleteTodo(id) {
      this.todos = this.todos.filter(todo => todo.id !== id);
    }
  }
};
</script>
```

### 参考リンク
- [リストレンダリング](https://jp.vuejs.org/v2/guide/list.html)

## vue-router
vue-routerとはvuejsが提供するシングルページアプリケーションにおいて、見せかけ上のページ遷移を作り出すための機能のこと。  

Vue.jsを始めとするイマドキのクライアントサイドフレームワークで開発できるシングルページアプリケーションではサーバーを用いずに、クライアント側だけでブラウザ上に表示するページを切り替えることができる。  

Vue.jsでは、そのための仕組み/機能がvue-routerであり、公式ツールvue-cliでは、vue-routerを組み込んだシングルページアプリケーションのテンプレートを用意してあるので、vue-routerを一緒にインストールするように選択するだけでよい。

### モード
vue-routerでは`hash`と`history`の2つのモードが存在する。  

デフォルトは`hash`モードで、URLは次のようにハッシュタグが付与された状態になる。  

```
http://localhost:9999/#/
```

このURLからハッシュタグを取り除くためには、`router.js`を次のように設定しvue-routerを`history`モードにする必要がある。  

[ router.js ]
```
export default new Router({
  mode: 'history',
  base: process.env.BASE_URL
});
```

これによりHTML5のHistoryAPIを使用したhistoryモードになり、URLからハッシュタグが取り除かれるが`history`モードにすることで起こりうる問題もある。  

**直接パラメータ付きのURLにアクセスした場合、404エラー（404Error）が発生する。**

vuejsはシングルページアプリケーションで画面遷移はクライアント側で行われるため、サーバに遷移後のURLが直接たたかれると404エラーになってしまう。  

もし`history`でサービスを提供する際は、`.htaccess`などでリライトするよう設定しておく必要があるので注意が必要だ。

### サンプル

[ router.js ]
```js
import Vue from 'vue'
import Router from 'vue-router'
import PageOne from './components/PageOne'
import PageTwo from './components/PageTwo'

Vue.use(Router)

export default new Router({
  mode: 'history', base: process.env.BASE_URL,
  routes: [
    { path: '/', component: PageOne },
    { path: '/pagetwo', component: PageTwo }
  ]
})
```

[ App.vue ]
```js
<template>
  <router-view></router-view>
</template>
```

[ PageOne.vue ]
```js
<template>
  <div>
    <h3>PageOne</h3>
    <router-link to="/pagetwo">PageTwo</router-link>
    <button v-on:click="toPageTwo()">To PageTwo</button>
  </div>
</template>

<script>
export default {
  methods: {
    toPageTwo() {
      this.$router.push("/pagetwo");
    }
  }
};
</script>
```

[ PageTwo.vue ]
```js
<template>
  <div>
    <h3>PageTwo</h3>
    <router-link to="/">PageOne</router-link>
    <button v-on:click="toPageOne()">To PageOne</button>
  </div>
</template>

<script>
export default {
  methods: {
    toPageOne() {
      this.$router.push("/");
    }
  }
};
</script>
```

### 参考リンク
- [VueJs: ルータ](http://www.shizuk.sakura.ne.jp/javascript/vuejs/router.html)
- [【Vue.js】historyモードにした際の注意点](【Vue.js】historyモードにした際の注意点)
- [HTML5 History モード](https://router.vuejs.org/ja/guide/essentials/history-mode.html#%E3%82%B5%E3%83%BC%E3%83%90%E3%83%BC%E3%81%AE%E8%A8%AD%E5%AE%9A%E4%BE%8B)

## ライフサイクルフック
Vue.jsにはいくつかのライフサイクルフック(lifecyclehooks)と呼ばれる関数が存在し、ある特定のタイミングで何か処理を行う場合に使用する。  

例えば、`created` フックはインスタンスが生成された後にコードを実行したいときに使われ、この他にもインスタンスのライフサイクルの様々な段階で呼ばれるフックがある。  

<img src="https://jp.vuejs.org/images/lifecycle.png">

### サンプル

```js
<script>
export default {
  methods: {
    toPageTwo() {
      this.$router.push("/pagetwo");
    }
  },
  created() {
    alert("PageOnecreated");
  },
  destroyed() {
    alert("PageOnedestroyed");
  }
};
</script>
```

### 参考リンク
- [インスタンスライフサイクルフック](https://jp.vuejs.org/v2/guide/instance.html#%E3%82%A4%E3%83%B3%E3%82%B9%E3%82%BF%E3%83%B3%E3%82%B9%E3%83%A9%E3%82%A4%E3%83%95%E3%82%B5%E3%82%A4%E3%82%AF%E3%83%AB%E3%83%95%E3%83%83%E3%82%AF)