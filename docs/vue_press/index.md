---
title: vuepress_概要・ノウハウ
date: 2019-09-19
description: vuepressによる静的サイトビルドノウハウ
---

## 概要
VuePressとはVueを使った静的サイトジェネレーターである。  
markdownを自動で静的ファイルにコンバートできるので、HTMLで作るページよりページコンテンツの作成コストが低い。  

またVueコンポーネントを使用することができる為、双方向データバインディングによる表示の切り替えや、
ディレクティブによるDOM操作も実現でき、さらに、markdown内でもVue,JSX,JS,CSSを組み込むことができる。  
本サイトもJSX,JS,CSSを組み込んだVueコンポーネントをmarkdownで呼び出すことで構成されている。


## vuepressの導入手順
### 前提
npm、Node.js (Ver 8 以上) がインストールされていること

### インストール

1. `cd [workdir]`
1. `npm init` //プロジェクトの初期化
1. `npm i -g vuepress` //vuepressをインストール
1. `vuepress dev [workdir]` //起動
1. `vuepress build [workdir]`  //ビルド(mdをhtmlに変換できる)

### 参考リンク

- [導入](https://app.codegrid.net/entry/2018-vuepress-1)
- [使い方](https://www.nxworld.net/services-resource/hello-vuepress.html)

## vuepressの設定ファイル
vuepressでは各種設定を`.vuepress`配下の`config.js`で行う。  
各種設定においてはまとめられたサイトが複数存在しているためリンクをまとめて記述は省略する。

- [VuePress入門](https://www.nxworld.net/services-resource/hello-vuepress.html)
- [VuePressの基礎基礎メモ](https://nogson2.hatenablog.com/entry/2019/07/04/191648)
- [VuePress の使い方](https://blog.ouvill.net/blog/2018-12-02--vuepress/vuepress/#vuepress-%E3%81%AE%E3%83%95%E3%82%A9%E3%83%AB%E3%83%80%E6%A7%8B%E6%88%90)

## Vueコンポーネント
<a href="../vuejs/">VueJs</a>を参照