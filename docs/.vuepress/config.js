module.exports = {
  base: '/vue_press/',
  docsDir: 'docs',
  lang: 'ja',
  title: 'Skills Box',
  description: 'Skills Know-How Contents',
  port: '9999',
  dest: 'build',

  themeConfig: {
    //ナビゲーションバー
    nav: [
      { text: 'Home', link: '/' },
      { text: 'Index', link: '/site_map/' },
      { text: 'Official', link: 'https://jp.vuejs.org/v2/guide/' }
    ],
    head: [
      [
        'link',
        {
          rel: 'stylesheet',
          href: `https://fonts.googleapis.com/css?family=Noto+Sans+JP`
        }
      ]
    ],
    // サイドバー
    sidebar: 'auto',
    collapsable: true,
    sidebarDepth: 2,
    //全ページに表示させる
    displayAllHeaders: true
  }


}