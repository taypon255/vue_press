---
title: network_トラブルシューティング
date: 2019-09-19
description: ネットワーク系のトラブルシューティングに役立つリンク
---

## ネットワークトラブル役立ちリンク集

- [確認くん](https://www.ugtop.com/spill.shtml)  
自分のグローバルIPアドレスの確認

- [ネットワーク系コマンドの一覧](http://cya.sakura.ne.jp/pc/ncmd.htm)  
Win、UNIX系のネットワーク系コマンドの一覧

- [tracertコマンドの詳細](http://harikofu.blog.fc2.com/blog-entry-896.html?sp)  
コマンドを実行した端末から対象ホストまでの通信の経路を表示してくれるコマンド